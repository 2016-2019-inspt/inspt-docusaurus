# inspt-docusaurus


###

pallete: 
#01aac1
#ffffff
#111111
#01d7f4
#017d8e
#e6e6e6



<div class="css-rozv0a" style="background-color: rgb(17, 17, 17);"><svg class="css-rt6ihi" viewBox="0 0 47.29729729729729 142.91123897409514"><g transform="translate(-10.193383331521723, -60.95642765633932) scale(10.193383331521723)" fill="#01aac1"><path d="M1 5.98 l4.64 0 l0 14.02 l-4.64 0 l0 -14.02 z"></path></g></svg><svg height="0" width="0" viewBox="0 0 0 0"><defs id="SvgjsDefs1517"></defs><g id="SvgjsG1518" featurekey="monogramFeature-0" transform="matrix(10.193383331521723,0,0,10.193383331521723,-10.193383331521723,-60.95642765633932)" fill="#01aac1"><path d="M1 5.98 l4.64 0 l0 14.02 l-4.64 0 l0 -14.02 z"></path></g><g id="SvgjsG1519" featurekey="fHLFJf-0" transform="matrix(1.6644591250157486,0,0,1.6644591250157486,65.33554087498425,51.90807787222877)" fill="#ffffff"><path d="M1 5.98 l4.64 0 l0 14.02 l-4.64 0 l0 -14.02 z M18.0595 9.38 q0.89 0.52 1.39 1.46 t0.5 2.18 l0 6.98 l-4.5 0 l0 -5.68 q0 -0.68 -0.36 -1.09 t-0.94 -0.41 q-0.66 0 -1.05 0.48 t-0.39 1.28 l0 5.42 l-4.5 0 l0 -10.94 l4.5 0 l0 1.56 q0.56 -0.86 1.41 -1.31 t1.91 -0.45 q1.14 0 2.03 0.52 z M27.869 8.69 q-0.27 0.25 -0.27 0.67 l0 0.3 l2.12 0 l0 2.84 l-2.12 0 l0 7.5 l-4.48 0 l0 -7.5 l-1.5 0 l0 -2.84 l1.5 0 l0 -0.98 q0 -1.7 1.22 -2.71 t3.26 -1.01 q1.34 0 2.6 0.38 l-0.44 3.32 q-0.8 -0.22 -1.18 -0.22 q-0.44 0 -0.71 0.25 z M39.9485 9.59 q1.4 0.69 2.16 1.96 t0.76 2.97 t-0.76 2.97 t-2.16 1.97 t-3.26 0.7 t-3.26 -0.7 t-2.16 -1.97 t-0.76 -2.97 t0.76 -2.97 t2.16 -1.96 t3.26 -0.69 t3.26 0.69 z M35.4985 13.04 q-0.45 0.58 -0.45 1.54 t0.45 1.54 t1.19 0.58 t1.19 -0.58 t0.45 -1.54 t-0.45 -1.54 t-1.19 -0.58 t-1.19 0.58 z M50.668000000000006 9.33 q0.85 -0.47 1.91 -0.47 l0 4.06 q-0.44 -0.06 -0.86 -0.06 q-1.16 0 -1.82 0.51 t-0.66 1.41 l0 5.22 l-4.5 0 l0 -10.94 l4.5 0 l0 1.6 q0.58 -0.86 1.43 -1.33 z M71.2275 9.38 q0.88 0.52 1.37 1.46 t0.49 2.18 l0 6.98 l-4.5 0 l0 -5.68 q0 -0.68 -0.34 -1.09 t-0.9 -0.41 q-0.62 0 -1 0.48 t-0.38 1.28 l0 5.42 l-4.5 0 l0 -5.68 q0 -0.68 -0.34 -1.09 t-0.9 -0.41 q-0.62 0 -1 0.48 t-0.38 1.28 l0 5.42 l-4.5 0 l0 -10.94 l4.5 0 l0 1.54 q0.56 -0.84 1.39 -1.29 t1.87 -0.45 q1.24 0 2.17 0.62 t1.37 1.72 q0.54 -1.12 1.45 -1.73 t2.13 -0.61 q1.12 0 2 0.52 z M84.487 9.93 q1.33 1.07 1.33 2.99 l0 7.08 l-4.42 0 l0 -1.18 q-1 1.38 -3.08 1.38 q-1.72 0 -2.69 -0.93 t-0.97 -2.51 q0 -1.62 1.08 -2.46 t3.16 -0.88 l2.5 0 q-0.02 -0.58 -0.45 -0.89 t-1.25 -0.31 q-0.64 0 -1.53 0.18 t-1.79 0.52 l-1 -3 q1.4 -0.52 2.79 -0.79 t2.61 -0.27 q2.38 0 3.71 1.07 z M80.727 17.12 q0.41 -0.22 0.67 -0.62 l0 -1 l-1.56 0 q-0.96 0 -0.96 0.88 q0 0.46 0.26 0.71 t0.74 0.25 q0.44 0 0.85 -0.22 z M95.9865 19.52 q-0.54 0.32 -1.41 0.5 t-1.77 0.18 q-1.84 0 -2.95 -1.01 t-1.11 -2.71 l0 -3.98 l-1.5 0 l0 -2.84 l1.5 0 l0 -3.06 l4.48 0 l0 3.06 l2.56 0 l0 2.84 l-2.56 0 l0 3.3 q0 0.44 0.24 0.69 t0.64 0.23 q0.38 0 1.12 -0.28 z M97.856 9.06 l4.5 0 l0 10.94 l-4.5 0 l0 -10.94 z M101.696 4.300000000000001 q0.64 0.64 0.64 1.64 t-0.64 1.64 t-1.64 0.64 t-1.64 -0.64 t-0.64 -1.64 t0.64 -1.64 t1.64 -0.64 t1.64 0.64 z M110.2055 12.54 q-0.66 0 -1.05 0.55 t-0.39 1.47 q0 0.94 0.39 1.49 t1.05 0.55 q1 0 1.34 -1.18 l3.64 1.68 q-0.6 1.46 -1.97 2.26 t-3.25 0.8 q-1.72 0 -3.02 -0.69 t-2.01 -1.95 t-0.71 -2.94 q0 -1.7 0.71 -2.99 t2.02 -1.99 t3.05 -0.7 q1.78 0 3.14 0.83 t2.04 2.33 l-3.58 1.52 q-0.44 -1.04 -1.4 -1.04 z M125.74499999999999 9.93 q1.33 1.07 1.33 2.99 l0 7.08 l-4.42 0 l0 -1.18 q-1 1.38 -3.08 1.38 q-1.72 0 -2.69 -0.93 t-0.97 -2.51 q0 -1.62 1.08 -2.46 t3.16 -0.88 l2.5 0 q-0.02 -0.58 -0.45 -0.89 t-1.25 -0.31 q-0.64 0 -1.53 0.18 t-1.79 0.52 l-1 -3 q1.4 -0.52 2.79 -0.79 t2.61 -0.27 q2.38 0 3.71 1.07 z M121.985 17.12 q0.41 -0.22 0.67 -0.62 l0 -1 l-1.56 0 q-0.96 0 -0.96 0.88 q0 0.46 0.26 0.71 t0.74 0.25 q0.44 0 0.85 -0.22 z M145.414 5.16 l0 14.84 l-4.5 0 l0 -1.1 q-1.08 1.26 -2.78 1.26 q-1.5 0 -2.64 -0.7 t-1.76 -1.99 t-0.62 -3.01 q0 -1.68 0.6 -2.94 t1.71 -1.94 t2.59 -0.68 q0.88 0 1.62 0.33 t1.28 0.95 l0 -5.02 l4.5 0 z M140.474 16.12 q0.44 -0.58 0.44 -1.54 t-0.44 -1.54 t-1.18 -0.58 t-1.19 0.58 t-0.45 1.54 t0.45 1.54 t1.19 0.58 t1.18 -0.58 z M156.8235 9.59 q1.4 0.69 2.16 1.96 t0.76 2.97 t-0.76 2.97 t-2.16 1.97 t-3.26 0.7 t-3.26 -0.7 t-2.16 -1.97 t-0.76 -2.97 t0.76 -2.97 t2.16 -1.96 t3.26 -0.69 t3.26 0.69 z M152.37349999999998 13.04 q-0.45 0.58 -0.45 1.54 t0.45 1.54 t1.19 0.58 t1.19 -0.58 t0.45 -1.54 t-0.45 -1.54 t-1.19 -0.58 t-1.19 0.58 z M166.79299999999998 12.54 q-0.66 0 -1.05 0.55 t-0.39 1.47 q0 0.94 0.39 1.49 t1.05 0.55 q1 0 1.34 -1.18 l3.64 1.68 q-0.6 1.46 -1.97 2.26 t-3.25 0.8 q-1.72 0 -3.02 -0.69 t-2.01 -1.95 t-0.71 -2.94 q0 -1.7 0.71 -2.99 t2.02 -1.99 t3.05 -0.7 q1.78 0 3.14 0.83 t2.04 2.33 l-3.58 1.52 q-0.44 -1.04 -1.4 -1.04 z M177.68249999999998 11.82 q-0.28 0 -0.45 0.1 t-0.17 0.26 q0 0.24 0.36 0.4 t1.2 0.38 q1.18 0.32 2.02 0.66 t1.47 1.04 t0.63 1.82 t-0.61 1.96 t-1.73 1.3 t-2.62 0.46 q-3.06 0 -5.34 -1.56 l1.36 -2.8 q2.2 1.36 4.04 1.36 q0.32 0 0.5 -0.09 t0.18 -0.23 q0 -0.22 -0.35 -0.37 t-1.13 -0.35 q-1.18 -0.3 -2 -0.64 t-1.45 -1.04 t-0.63 -1.84 t0.6 -1.99 t1.69 -1.32 t2.53 -0.47 q2.62 0 5.08 1.38 l-1.48 2.78 q-2.44 -1.2 -3.7 -1.2 z"></path></g></svg></div>


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/2016-2019-inspt/inspt-docusaurus.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/2016-2019-inspt/inspt-docusaurus/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.












# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

Using SSH:

```
$ USE_SSH=true yarn deploy
```

Not using SSH:

```
$ GIT_USER=<Your GitHub username> yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
